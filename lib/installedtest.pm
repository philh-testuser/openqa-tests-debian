package installedtest;

use strict;

use base 'basetest';

# base class for tests that run on installed system

# should be used when with tests, where system is already installed, e. g all parts
# of upgrade tests, postinstall phases...

use testapi;
use utils;

sub root_console {
    # Switch to a default or specified TTY and log in as root.
    my $self = shift;
    my %args = (
        tty => 1,    # what TTY to login to
        @_);

    send_key "ctrl-alt-f$args{tty}";

    # Scenarios:
    # * Root login is configured and password is provided:
    #   * ROOT_PASSWORD must be set
    # * Root login is not configured, but a user with sudo rights exists (e.g. live images):
    #   * auto login, passwordless sudo -> CONSOLE_AUTOLOGIN=1
    #   * auto login, sudo requires password -> CONSOLE_AUTOLOGIN=1, USER_PASSWORD must be set
    #   * manual login -> USER_LOGIN and USER_PASSWORD must be set
    # * Root login is configured:
    #   * ROOT_PASSWORD can be set
    if (get_var('ROOT_PASSWORD') && !check_var('ROOT_PASSWORD', 'LEAVE-BLANK')) {
        console_login;
    }
    else {
        if (check_var('CONSOLE_AUTOLOGIN', 1)) {
            assert_screen ["root_console", "user_console"], 30;
            if (match_has_tag "root_console") {
                return;    # Already root
            }
        }
        else {
            console_login(user     => get_var(   "USER_LOGIN", "testy"),
                          password => get_var("USER_PASSWORD", "weakpassword"));
        }
        type_safely "sudo -s\n";
        if (!check_var('CONSOLE_AUTOLOGIN', 1)) {
            assert_screen ["sudo-passwordprompt", "root_console"], 3;
            if (match_has_tag "sudo-passwordprompt") {
                type_password get_var("USER_PASSWORD", "weakpassword");
                send_key 'ret';
            }
        }
        assert_screen "root_console", 3;
    }
}

sub post_fail_hook {
    my $self = shift;

    if (check_screen 'emergency_rescue', 3) {
        my $password = get_var("ROOT_PASSWORD", "weakpassword");
        type_string "$password\n";
        # bring up network so we can upload logs
        assert_script_run "dhclient";
    }
    else {
        $self->root_console(tty => 6);
    }

    # kludge the keyboard to avoid typing @ instead of " and the like
    console_loadkeys_us;

    # check if we're still producing a syslog file
    my $has_syslog = (0 == (script_run("test -f /var/log/syslog") // 1));

    # First, let's check for the running out of space:
    my $check_nospace = $has_syslog
      ? "grep -n 'No space left on device' /var/log/syslog"
      : "journalctl --grep 'No [s]pace left on device'";
    if (0 == (script_run($check_nospace .  " > /dev/${serialdev}", timeout => 60) // 1)) {
        record_info("No Space", "'No space left on device' seen in " . $has_syslog ? "syslog" : "journal", result => 'fail');
    }

    script_run("df -h > /tmp/df.txt");
    script_run("dpkg -l > /tmp/installed-pkgs.txt");
    if (make_sure_curl_is_available()) {
        upload_logs '/tmp/df.txt';
        upload_logs '/var/log/Xorg.0.log',   log_name => 'Xorg.0.log.txt', failok => 1;
        upload_logs '/etc/apt/sources.list', log_name => 'sources.list.txt';
        if ($has_syslog) {
            upload_logs '/var/log/syslog',       log_name => 'syslog.txt';
        }
        else {
            script_run("journalctl -b > /tmp/journal.txt");
            upload_logs '/tmp/journal.txt';

            my $user_login    = get_var("USER_LOGIN", "testy");
            my $user_log_file = "/tmp/journal_user:$user_login.txt";
            script_run("su $user_login -c journalctl -q > $user_log_file");
            upload_logs "$user_log_file";
        }
        upload_logs '/tmp/installed-pkgs.txt';

        # Upload /var/log
        if (0 == (script_run "type gzip && tar -cvzf /tmp/var_log.tar.gz /var/log" // 1)) {
            upload_logs "/tmp/var_log.tar.gz";
        }
        elsif (0 == (script_run "tar -cvf /tmp/var_log.tar /var/log" // 1)) {
            upload_logs "/tmp/var_log.tar";
        }

        # Sometimes useful for diagnosing FreeIPA issues
        upload_logs "/etc/nsswitch.conf", log_name => 'nsswitch.conf.txt', failok => 1;
    }
    else {
        # splurge these at the serial port, to get it into the OpenQA logs without curl
        script_run("cat /tmp/df.txt > /dev/${serialdev}");
        script_run("cat /var/log/Xorg.0.log > /dev/${serialdev}");
        script_run("cat /etc/apt/sources.list 2>/dev/null > /dev/${serialdev}");
        if ($has_syslog) {
            script_run("tail -2000 /var/log/syslog > /dev/${serialdev}");
        }
        else {
            script_run("journalctl -b > /dev/${serialdev}");
        }
        script_run("cat /tmp/installed-pkgs.txt > /dev/${serialdev}");
    }
}

1;

# vim: set sw=4 et:
