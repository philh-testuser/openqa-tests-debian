#!/usr/bin/perl

use strict;
use warnings;

die "$0: too many arguments\n" if defined($ARGV[1]);

my ($namespace, $job_id, $iso_name);

$namespace = $ARGV[0] ;

if ($ENV{SSH_ORIGINAL_COMMAND} =~ m%^([\w-]+)\s+([\w./-]+)\s+(\d+\b)\s*(\b[\w./-]*).*$%) {
    die "I don't like your command" unless ("salsa-job-launcher" eq $1);
    if (defined($namespace)) {
        die "namespace mismatch" unless ($namespace eq $2);
    }
    else {
        $namespace = $2;
    }
    $job_id = $3;
    $iso_name = ("" eq $4) ? 'gtk-mini.iso' : $4 ;
}
else {
    die "your command doesn't match what's allowed [" . $ENV{SSH_ORIGINAL_COMMAND} . "]\n";
}

my $url   =
    "https://salsa.debian.org/" . ${namespace} .
    "/debian-installer/-/jobs/" . ${job_id} .
    "/artifacts/raw/public/" . ${iso_name} ;
my $build = "salsa-" . ${namespace} . "-" . ${job_id} ;
my $dest  = ${build} . "-" . ${iso_name} ;

my $command =
    '/usr/share/openqa/script/client -v isos post ' .
    'ISO_URL="' . $url . '" ISO="' . $dest .
    '" DISTRI=debian VERSION=testing FLAVOR=mini-iso ARCH=x86_64 ' .
    'BUILD=-' . $build;

printf "$0: running: %s\n", $command;
system($command);
