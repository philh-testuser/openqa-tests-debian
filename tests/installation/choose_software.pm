# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C)      2017 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;
use utils;
use List::MoreUtils qw(first_index);

my $unselected_gui_default = check_var("MACHINE", "aarch64");

sub run {
    # FIXME -- this should be a bit more definite than fiddling with timeouts
    my $cs_timeout   = check_var('FLAVOR', 'mini-iso') ? 600 : 60;
    my @desktop_list = qw(
      no_desktop
      gnome
      xfce
      gnome_flashback
      kde
      cinnamon
      mate
      lxde
      lxqt
    );

    assert_screen('ChooseSoftware', $cs_timeout);
    # gnome_flashback added to debian 11 desktop list,
    # so provide an opt-out flag for use with older versions
    if (match_has_tag('gnome_flashback_unlisted')) {
        my $fb_index = first_index { $_ eq "gnome_flashback" } @desktop_list;
        splice @desktop_list, $fb_index, 1;
    }
    my $desktop       = get_var('DESKTOP', 'no_desktop');
    my $extra_desktop = get_var('EXTRA_DESKTOP');
    my $desktop_index = first_index { $_ eq $desktop } @desktop_list;

    if (check_var('DI_UI', 'speech')) {
        type_string sprintf "1 %d 12\n", $desktop_index + 1;
    }
    else {

        if ($desktop eq 'no_desktop') {
            # de-select "Desktop environment", and let !..._selected deselect Gnome
            send_key 'spc' unless ($unselected_gui_default);
            send_key 'down';
        }
        else {
            for (my $i = 0; $i < $desktop_index; $i++) {
                if ($desktop_list[$i] eq 'gnome') {
                    # deselect gnome if not wanted
                    if ($desktop ne 'gnome' and $extra_desktop ne 'gnome') {
                        # has been seen to fail, so let's try harder
                        send_key_until_needlematch('gnome_deselected', 'spc') unless ($unselected_gui_default);
                    }
                }
                elsif ($desktop_list[$i] eq $extra_desktop) {
                    # select extra desktop, if wanted
                    send_key 'spc';
                }
                send_key 'down';
            }
        }
        # 'no_desktop' is a special case in the above, as its value of 0
        # means that hitting return below unsets "Desktop env..."

        if (!check_screen $desktop . '_selected', 1) {
            send_key 'spc';
        }
        assert_screen $desktop . '_selected' if $desktop;

        # were previously looking for '$desktop' here, if it was not in the above list

        send_key 'tab' if ('gtk' eq get_var('DI_UI', 'gtk'));
        send_key 'ret';
    }
}

sub test_flags {
    return {fatal => 1};
}

1;
# vim: set sw=4 et:
