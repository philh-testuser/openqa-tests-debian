# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C)      2017 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;

sub run {
    # wait for bootloader to appear
    assert_screen 'bootloader';

    # here we could select text/gui mode, and throw things at the kernel cmdline

#    if (get_var('INSTALLONLY')) {
#        send_key 'tab';
#        type_string 'debian-installer/exit/poweroff=true ';
#    }

    # press enter to boot right away
    send_key 'ret';
}

sub test_flags {
    return { fatal => 1 };
}

1;
# vim: set sw=4 et:
