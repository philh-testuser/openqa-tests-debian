# Copyright (C)      2023 Roland Clobus
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "installedtest";
use strict;
use testapi;
use utils;

sub run {
    # Precondition: the live image has been started
    # Precondition: (not explicitly enforced): a fully blank HD image
    # Postcondition: Installation to the HD, wiping all data
    # Postcondition: Rebooted

    my $user_full_name = get_required_var('INSTALLER_USER_FULL_NAME');
    my $user_login     = get_required_var('INSTALLER_USER_LOGIN');
    my $user_password  = get_required_var('INSTALLER_USER_PASSWORD');

    # Start Calamares
    # TODO: alternative startup via click on icon
    if (is_debian_bookworm()) {
        menu_launch_type 'install-debian';
    } else {
        menu_launch_type 'calamares-install-debian';
    }
    assert_and_click 'calamares_started', timeout => 90;

    # On MATE, the final screen does not fit -> maximise
    if (get_var('DESKTOP', '') eq 'mate') {
        send_key 'alt-f10';
        wait_still_screen(stilltime => 2);
    }

    # Walk through the Calamares installer (Next...Finish)
    assert_and_click 'calamares_location_configured';
    assert_and_click 'calamares_keyboard_configured';
    assert_screen 'calamares_partition';
    assert_and_click 'calamares_partition_select_wipe_all';
    assert_and_click 'calamares_partition_configured';
    assert_screen 'calamares_username';
    type_string $user_full_name;
    send_key 'tab';
    type_string $user_login;
    send_key 'tab';
    type_string 'debian';
    send_key 'tab';
    type_string $user_password;
    send_key 'tab';
    type_string $user_password;
    send_key 'tab';
    assert_and_click 'calamares_user_configured';
    wait_screen_change { assert_and_click 'calamares_summary_confirmed'; };
    if (check_screen 'calamares_summary_confirmed', 1) {
        # if this still matches, the click(s) were apparently wasted because of
        # a popup, so let's try using a keyboard shortcut instead.
        wait_screen_change { send_key 'alt-i'; };
    }
    # Installation can take a long time
    my $installation_finished = 0;
    foreach (1 .. 30) {
        if (check_screen 'calamares_installation_finished', 60) {
            $installation_finished = 1;
            last;
        }
        # Try to avoid screen savers, power management etc.
        send_key 'shift', wait_screen_change => 0;
    }
    if (!$installation_finished) {
        # Not finished in time -> fail
        assert_screen 'calamares_installation_finished';
    }
    #assert_and_click 'calamares_turn_off_reboot';
    assert_and_click 'calamares_finish';
    # Wait until Calamares has exited
    wait_still_screen(stilltime => 2);

    # For all following testing steps:
    # - boot from HD, not from the live image (eject the CD)
    # - use the new credentials, not the credentials from the live image
    set_var('BOOT_LIVE',         0);                # For _graphical_wait_login.pm
    set_var('USER_LOGIN',        $user_login);
    set_var('USER_PASSWORD',     $user_password);
    set_var('IS_LIVE',           1);                # Needed for root via sudo (installedtest.pm)
    set_var('CONSOLE_AUTOLOGIN', 0);                # Calamares configures sudo
    set_var('_SETUP_DONE',       0);                # The welcome screen will be shown again

    eject_cd;
    power('reset');
}

sub test_flags {
    return {fatal => 1};
}

1;
# vim: set sw=4 et:
