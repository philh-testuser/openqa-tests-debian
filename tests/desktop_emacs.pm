use base "installedtest";
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;

    # throw a delay in, to ensure that we've a settled state in the desktop
    # before switching away from it, in the hope that lets us get back here
    wait_still_screen(stilltime=>5, similarity_level=>45);

    # Prepare the environment:
    #
    # Become root
    $self->root_console(tty=>3);
    console_loadkeys_us;

    # make sure emacs is installed
    assert_script_run "apt install -y emacs", 180;
    # Leave the root terminal and switch back to desktop.
    desktop_vt();

    # Run emacs via Alt-F2
    wait_screen_change { send_key "alt-f2"; };
    wait_still_screen(stilltime=>5, similarity_level=>45);

    type_very_safely "emacs\n";
    wait_still_screen(stilltime=>5, similarity_level=>44);
    if (check_screen "console_instead_of_GUI", 1) {
        die "failed to switch back to GUI";
    }

    assert_screen "emacs_splashscreen";
    send_key "alt-x";
    type_safely "hanoi\n";
    assert_screen "emacs_hanoi_solved";
}


sub test_flags {
    return { fatal => 1 };
}

1;

# vim: set sw=4 et:
