use base "installedtest";
use strict;
use testapi;
use utils;

# This test checks that KDE Partition Manager starts.

sub run {
    my $self = shift;

    # Start the application
    menu_launch_type 'partition editor';

    # In the live environment no root password is required
    if (get_var('IS_LIVE') && get_var('BOOT_LIVE')) {
        if (check_screen "auth_required", 15) {
            # bookworm still prompts for this
            type_very_safely(get_var("USER_PASSWORD", "live"));
            send_key 'ret';
            wait_still_screen 2;
        }
    }
    else {
        assert_screen "auth_required";

        # Provide root password to run the application
        type_very_safely(get_var("ROOT_PASSWORD", "weakpassword"));
        send_key 'ret';
        wait_still_screen 2;
    }

    # Check that it is started
    assert_screen 'kparted_runs';

    # Close the application
    quit_with_shortcut();
}

sub test_flags {
    return {always_rollback => 1};
}

1;

# vim: set sw=4 et:
