use base "installedtest";
use strict;
use testapi;
use utils;

sub run {
    my $self      = shift;
    my $user_home = "/home/" . get_required_var("USER_LOGIN");
    my $pdf_dest  = $user_home . "/" . (check_var("DISTRI", "fedora") ? "Desktop" : "PDF");

    # throw a delay in, to ensure that we've a settled state in the desktop
    # before switching away from it, in the hope that lets us get back here
    wait_still_screen(stilltime => 5, similarity_level => 45);

    # Prepare the environment:
    #
    # Become root
    $self->root_console(tty => 3);
    console_loadkeys_us;

    # Create a text file with content to print
    script_run "cd $user_home";
    assert_script_run "echo 'A quick brown fox jumps over a lazy dog.' > testfile.txt";
    script_run "chmod 666 testfile.txt";
    # Install the Cups-PDF package to use the Cups-PDF printer
    assert_script_run "apt install -y printer-driver-cups-pdf", 180;
    # Leave the root terminal and switch back to desktop.
    desktop_vt();
    my $desktop = get_var("DESKTOP");
    # some simple variances between desktops. defaults are for GNOME
    my $editor = "gnome-text-editor";
    # GNOME pre-v42 used gedit
    $editor = "gedit" if (get_var("GNOMEVERSION", 42) < 42);
    my $viewer   = "evince";
    my $maximize = "super-up";
    if ($desktop eq "cinnamon") {
        $editor   = "gedit";     # The package cinnamon-desktop-environment prefers gedit over gnome-text-editor
        $viewer   = "evince";
        $maximize = "alt-f10";
    } elsif ($desktop eq "kde") {
        $editor   = "kwrite";
        $viewer   = "okular";
        $maximize = "super-pgup";
    } elsif ($desktop eq "lxde") {
        $editor   = "mousepad";
        $viewer   = "evince";
        $maximize = "f11";        # Not really maximize, but full-screen
    } elsif ($desktop eq "lxqt") {
        $editor   = "featherpad";
        $viewer   = "qpdfview";
        $maximize = "super-pgup";
    } elsif ($desktop eq "mate") {
        $editor   = "pluma";
        $viewer   = "atril";
        $maximize = "alt-f10";
    } elsif ($desktop eq "xfce") {
        $editor   = "mousepad";
        $viewer   = "atril";
        $maximize = "alt-f10";
    }

    # Open the text editor and print the file.
    wait_still_screen(stilltime => 5, similarity_level => 45);
    menu_launch_type "$editor $user_home/testfile.txt";
    wait_still_screen(stilltime => 5, similarity_level => 44);
    if (check_screen "console_instead_of_GUI", 1) {
        die "failed to switch back to GUI";
    }
    # Print the file using the Cups-PDF printer
    hold_key 'ctrl';
    wait_screen_change { send_key "p"; };
    release_key 'ctrl';
    wait_still_screen(stilltime => 5, similarity_level => 45);
    check_screen [qw(printing_pdfprinter_ready printing_select_pdfprinter)];
    if (match_has_tag "printing_select_pdfprinter") {
        record_soft_failure "PDF printer was not selected by default";
        assert_and_click "printing_select_pdfprinter";
    }
    else {
        assert_screen "printing_pdfprinter_ready";
    }
    wait_still_screen(stilltime => 2, similarity_level => 45);
    assert_and_click "printing_print";
    # Exit the application
    send_key "alt-f4";
    # Wait out confirmation on GNOME
    if (check_screen "printing_print_completed", 1) {
        sleep 30;
    }

    # Open the pdf file and check the print
    menu_launch_type "bash -c '$viewer $pdf_dest/*-job_1.pdf'";
    wait_still_screen(stilltime => 5);
    # Resize the window, so that the size of the document fits the bigger space
    # and gets more readable.
    send_key $maximize;
    wait_still_screen(stilltime => 2);
    # make sure we're at the start of the document
    send_key "ctrl-home" if ($desktop eq "kde");
    # Check the printed pdf.
    assert_screen "printing_check_sentence";
}


sub test_flags {
    return {fatal => 1};
}

1;

# vim: set sw=4 et:
